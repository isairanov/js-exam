class Tab {
  constructor(content) {
    this.content = content;
  }

  render() {
    const tab = document.createElement('div');

    const tabItems = document.createElement('div');
    tabItems.classList.add('tab__items');

    const tabText = document.createElement('div');

    for (let i = 0; i < this.content.length; i++) {
      const item = document.createElement('div');
      item.classList.add('tab__item');
      item.textContent = `${this.content[i].title}`;

      item.addEventListener('click', () => {
        tabText.textContent = `${this.content[i].data}`;
        const selectedItem = document.querySelector('._current');
        if (selectedItem) {
          selectedItem.classList.remove('_current');
        }
        item.classList.add('_current');
      })
      tabItems.appendChild(item);
    }

    tab.append(tabItems, tabText);
    return tab;
  }
}

// Входные данные
const tab = new Tab([
  {
    title: 'Общая информация',
    data: `Какая-то личная информация о пользователе`
  },
  {
    title: 'История',
    data: `Какая-то история действий пользователя`
  },
  {
    title: 'Друзья',
    data: `Какие-то люди`
  }
])

// Вставляем Таб на страницу и вызываем метод рэендер
document.getElementById('tab').appendChild(tab.render())
